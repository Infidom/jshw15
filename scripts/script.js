let number;

do {
    number = prompt("Enter the number", [number]);
} while (isNaN(number) || number == null || number.trim().length === 0 || number % 1 !== 0);

function calcFactorial(a) {
    let factorial = 1;
    for (i = 1; i <= a; i++) {
        factorial *= i;
    }
    alert(`The factorial of ${a} is ${factorial}`);
}

calcFactorial(number);